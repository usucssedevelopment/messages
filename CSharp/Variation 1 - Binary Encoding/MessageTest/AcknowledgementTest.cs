﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Messages;

namespace MessageTest
{
    [TestClass]
    public class AcknowledgementTest
    {
        [TestMethod]
        public void Acknowledgement_TestNormalMessage()
        {
            var message = new Acknowledgement() {Id = 10, RequestId = 2 };
            var bytes = message.Encode();
            
            Assert.AreEqual(9, bytes.Length);
            Assert.AreEqual(1, bytes[0]);
            Assert.AreEqual(0, bytes[1]);
            Assert.AreEqual(0, bytes[2]);
            Assert.AreEqual(0, bytes[3]);
            Assert.AreEqual(10, bytes[4]);
            Assert.AreEqual(0, bytes[5]);
            Assert.AreEqual(0, bytes[6]);
            Assert.AreEqual(0, bytes[7]);
            Assert.AreEqual(2, bytes[8]);

            var message2 = Message.Decode(bytes) as Acknowledgement;
            Assert.IsNotNull(message2);
            Assert.AreEqual(message.Id, message2.Id);
            Assert.AreEqual(message.RequestId, message2.RequestId);
        }

        [TestMethod]
        public void Acknowledgement_TestBadDecoding()
        {
            var message = new Acknowledgement() { Id = 10, RequestId = 2 };
            var bytes = message.Encode();
            TestUtils.CheckBadDecodings(bytes);
        }


    }
}
