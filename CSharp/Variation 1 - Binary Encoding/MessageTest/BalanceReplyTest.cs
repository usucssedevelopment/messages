﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Messages;

namespace MessageTest
{
    [TestClass]
    public class BalanceReplyTest
    {
        [TestMethod]
        public void BalanceReply_TestNormalMessage()
        {
            var message = new BalanceReply() { Id = 10, RequestId = 2, Balance  = 17L };
            var bytes = message.Encode();

            Assert.AreEqual(17, bytes.Length);
            Assert.AreEqual(2, bytes[0]);
            Assert.AreEqual(0, bytes[1]);
            Assert.AreEqual(0, bytes[2]);
            Assert.AreEqual(0, bytes[3]);
            Assert.AreEqual(10, bytes[4]);
            Assert.AreEqual(0, bytes[5]);
            Assert.AreEqual(0, bytes[6]);
            Assert.AreEqual(0, bytes[7]);
            Assert.AreEqual(2, bytes[8]);
            Assert.AreEqual(0, bytes[9]);
            Assert.AreEqual(0, bytes[10]);
            Assert.AreEqual(0, bytes[11]);
            Assert.AreEqual(0, bytes[12]);
            Assert.AreEqual(0, bytes[13]);
            Assert.AreEqual(0, bytes[14]);
            Assert.AreEqual(0, bytes[15]);
            Assert.AreEqual(17, bytes[16]);

            var message2 = Message.Decode(bytes) as BalanceReply;
            Assert.IsNotNull(message2);
            Assert.AreEqual(message.Id, message2.Id);
            Assert.AreEqual(message.RequestId, message2.RequestId);
            Assert.AreEqual(message.Balance, message2.Balance);
        }

        [TestMethod]
        public void BalanceReply_TestBadDecoding()
        {
            var message = new BalanceReply() { Id = 10, RequestId = 2, Balance = 17L };
            var bytes = message.Encode();
            TestUtils.CheckBadDecodings(bytes);
        }

    }
}
