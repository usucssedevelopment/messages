﻿using Messages;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MessageTest
{
    /// <summary>
    /// Summary description for ErrorReplyTest
    /// </summary>
    [TestClass]
    public class ErrorReplyTest
    {
        [TestMethod]
        public void ErrorReply_TestNormalMessage()
        {
            var message = new ErrorReply() { Id = 10, RequestId = 2, Reason = "ABC"};
            var bytes = message.Encode();

            Assert.AreEqual(17, bytes.Length);
            Assert.AreEqual(3, bytes[0]);
            Assert.AreEqual(0, bytes[1]);
            Assert.AreEqual(0, bytes[2]);
            Assert.AreEqual(0, bytes[3]);
            Assert.AreEqual(10, bytes[4]);
            Assert.AreEqual(0, bytes[5]);
            Assert.AreEqual(0, bytes[6]);
            Assert.AreEqual(0, bytes[7]);
            Assert.AreEqual(2, bytes[8]);
            Assert.AreEqual(0, bytes[9]);
            Assert.AreEqual(6, bytes[10]);
            Assert.AreEqual(0, bytes[11]);
            Assert.AreEqual(65, bytes[12]);
            Assert.AreEqual(0, bytes[13]);
            Assert.AreEqual(66, bytes[14]);
            Assert.AreEqual(0, bytes[15]);
            Assert.AreEqual(67, bytes[16]);

            var message2 = Message.Decode(bytes) as ErrorReply;
            Assert.IsNotNull(message2);
            Assert.AreEqual(message.Id, message2.Id);
            Assert.AreEqual(message.RequestId, message2.RequestId);
            Assert.AreEqual(message.Reason, message2.Reason);
        }

        [TestMethod]
        public void ErrorReply_TestBlankReason()
        {
            var message = new ErrorReply() { Id = 10, RequestId = 2, Reason = null };
            var bytes = message.Encode();

            Assert.AreEqual(11, bytes.Length);
            Assert.AreEqual(3, bytes[0]);
            Assert.AreEqual(0, bytes[1]);
            Assert.AreEqual(0, bytes[2]);
            Assert.AreEqual(0, bytes[3]);
            Assert.AreEqual(10, bytes[4]);
            Assert.AreEqual(0, bytes[5]);
            Assert.AreEqual(0, bytes[6]);
            Assert.AreEqual(0, bytes[7]);
            Assert.AreEqual(2, bytes[8]);
            Assert.AreEqual(0, bytes[9]);
            Assert.AreEqual(0, bytes[10]);

            var message2 = Message.Decode(bytes) as ErrorReply;
            Assert.IsNotNull(message2);
            Assert.AreEqual(message.Id, message2.Id);
            Assert.AreEqual(message.RequestId, message2.RequestId);
            Assert.AreEqual("", message2.Reason);
        }

        [TestMethod]
        public void ErrorReply_TestBadDecoding()
        {
            var message = new ErrorReply() { Id = 10, RequestId = 2, Reason = "ABC" };
            var bytes = message.Encode();
            TestUtils.CheckBadDecodings(bytes);
        }
    }
}
