﻿using Messages;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MessageTest
{
    /// <summary>
    /// Summary description for GetBalanceRequestTest
    /// </summary>
    [TestClass]
    public class GetBalanceRequestTest
    {
        [TestMethod]
        public void GetBalanceRequest_TestNormalMessage()
        {
            var message = new GetBalanceRequest() { Id = 2, AccountNumber = "A001"};
            var bytes = message.Encode();

            Assert.AreEqual(15, bytes.Length);
            Assert.AreEqual(4, bytes[0]);
            Assert.AreEqual(0, bytes[1]);
            Assert.AreEqual(0, bytes[2]);
            Assert.AreEqual(0, bytes[3]);
            Assert.AreEqual(2, bytes[4]);
            Assert.AreEqual(0, bytes[5]);
            Assert.AreEqual(8, bytes[6]);
            Assert.AreEqual(0, bytes[7]);
            Assert.AreEqual(97, bytes[8]);
            Assert.AreEqual(0, bytes[9]);
            Assert.AreEqual(48, bytes[10]);
            Assert.AreEqual(0, bytes[11]);
            Assert.AreEqual(48, bytes[12]);
            Assert.AreEqual(0, bytes[13]);
            Assert.AreEqual(49, bytes[14]);

            var message2 = Message.Decode(bytes) as GetBalanceRequest;
            Assert.IsNotNull(message2);
            Assert.AreEqual(message.Id, message2.Id);
            Assert.AreEqual(message.AccountNumber.ToLower(), message2.AccountNumber);

        }

        [TestMethod]
        public void GetBalanceRequest_TestBlankAccountNumber()
        {
            var message = new GetBalanceRequest() { Id = 2, AccountNumber = null };
            var bytes = message.Encode();

            Assert.AreEqual(7, bytes.Length);
            Assert.AreEqual(4, bytes[0]);
            Assert.AreEqual(0, bytes[1]);
            Assert.AreEqual(0, bytes[2]);
            Assert.AreEqual(0, bytes[3]);
            Assert.AreEqual(2, bytes[4]);
            Assert.AreEqual(0, bytes[5]);
            Assert.AreEqual(0, bytes[6]);

            var message2 = Message.Decode(bytes) as GetBalanceRequest;
            Assert.IsNotNull(message2);
            Assert.AreEqual(message.Id, message2.Id);
            Assert.AreEqual("", message2.AccountNumber);

        }

        [TestMethod]
        public void GetBalanceRequest_TestBadDecoding()
        {
            var message = new GetBalanceRequest() { Id = 2, AccountNumber = "A001" };
            var bytes = message.Encode();
            TestUtils.CheckBadDecodings(bytes);
        }

    }
}
