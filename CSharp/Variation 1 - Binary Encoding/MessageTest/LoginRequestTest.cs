﻿using System;
using Messages;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MessageTest
{
    [TestClass]
    public class LoginRequestTest
    {
        [TestMethod]
        public void LoginRequest_TestNormalMessage()
        {
            var expectedBytes = new byte[] {5, 0, 0, 0, 2, 0, 6, 0, 97, 0, 98, 0, 99, 0, 6, 0, 65, 0, 98, 0, 67};
            var message = new LoginRequest() { Id = 2, Username = " Abc   ", Password = "   AbC "};
            var bytes = message.Encode();

            Assert.AreEqual(expectedBytes.Length, bytes.Length);
            for (var i=0; i<bytes.Length; i++)
                Assert.AreEqual(expectedBytes[i], bytes[i], $"Failed on index={i}, with expected={expectedBytes[i]} and actual={bytes[i]}");

            var message2 = Message.Decode(bytes) as LoginRequest;
            Assert.IsNotNull(message2);
            Assert.AreEqual(message.Id, message2.Id);
            Assert.AreEqual(message.Username.Trim().ToLower(), message2.Username);
            Assert.AreEqual(message.Password.Trim(), message2.Password);
        }

        [TestMethod]
        public void LoginRequest_TestBlankUsernameAndPassword()
        {
            var expectedBytes = new byte[] { 5, 0, 0, 0, 2, 0, 0, 0, 0 };
            var message = new LoginRequest() { Id = 2, Username = null, Password = null };
            var bytes = message.Encode();

            Assert.AreEqual(expectedBytes.Length, bytes.Length);
            for (var i = 0; i < bytes.Length; i++)
                Assert.AreEqual(expectedBytes[i], bytes[i], $"Failed on index={i}, with expected={expectedBytes[i]} and actual={bytes[i]}");

            var message2 = Message.Decode(bytes) as LoginRequest;
            Assert.IsNotNull(message2);
            Assert.AreEqual(message.Id, message2.Id);
            Assert.AreEqual("", message2.Username);
            Assert.AreEqual("", message2.Password);
        }

        [TestMethod]
        public void LoginRequest_TestBadDecoding()
        {
            var message = new LoginRequest() { Id = 2, Username = "Abc", Password = "AAbbCC" };
            var bytes = message.Encode();
            TestUtils.CheckBadDecodings(bytes);
        }

    }
}
