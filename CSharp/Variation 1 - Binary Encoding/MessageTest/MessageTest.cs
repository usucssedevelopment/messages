﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Messages;

namespace MessageTest
{
    [TestClass]
    public class MessageTest
    {
        [TestMethod]
        public void Message_TestDecodeWithBadMessageTypeCode()
        {
            var badBytes = new byte[] {99, 0, 0, 0, 1};
            try
            {
                Message.Decode(badBytes);
                Assert.Fail("Expected an exception");
            }
            catch (ApplicationException)
            {
                // Ignore
            }
        }
    }
}
