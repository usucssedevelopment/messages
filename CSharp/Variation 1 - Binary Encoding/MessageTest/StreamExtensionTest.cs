﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Messages;

namespace MessageTest
{
    [TestClass]
    public class StreamExtensionTest
    {
        [TestMethod]
        public void StreamExtension_TestWriteByte()
        {
            var stream = new MemoryStream();
            for (byte i=0; i<100; i++)
                stream.Write(i);

            var bytes = stream.ToArray();
            Assert.AreEqual(100, bytes.Length);
            for (byte i= 0; i<100; i++)
                Assert.AreEqual(i, bytes[i]);
        }

        [TestMethod]
        public void StreamExtension_TestWriteShort()
        {
            short[] sampleNumbers = {0, 40, 127, 200, 350, 1000, 31003};
            var stream = new MemoryStream();
            foreach (var t in sampleNumbers)
                stream.Write(t);

            var bytes = stream.ToArray();
            Assert.AreEqual(sampleNumbers.Length*2, bytes.Length);

            var numberIndex = 0;
            for (var i = 0; i < bytes.Length; i += 2)
            {
                Assert.AreEqual((byte)(sampleNumbers[numberIndex] >> 8), bytes[i]);
                Assert.AreEqual((byte)(sampleNumbers[numberIndex] & 255), bytes[i + 1]);
                numberIndex++;
            }
        }

        [TestMethod]
        public void StreamExtension_TestWriteInt()
        {
            int[] sampleNumbers = { 0, 40, 127, 200, 350, 1000, 31003, 52356, 23523522 };
            var stream = new MemoryStream();
            foreach (var t in sampleNumbers)
                stream.Write(t);

            var bytes = stream.ToArray();
            Assert.AreEqual(sampleNumbers.Length * 4, bytes.Length);

            var numberIndex = 0;
            for (var i = 0; i < bytes.Length; i += 4)
            {
                Assert.AreEqual((byte)(sampleNumbers[numberIndex] >> 24), bytes[i]);
                Assert.AreEqual((byte)(sampleNumbers[numberIndex] >> 16), bytes[i + 1]);
                Assert.AreEqual((byte)(sampleNumbers[numberIndex] >> 8), bytes[i + 2]);
                Assert.AreEqual((byte)(sampleNumbers[numberIndex] & 255), bytes[i + 3]);
                numberIndex++;
            }
        }

        [TestMethod]
        public void StreamExtension_TestWriteLong()
        {
            long[] sampleNumbers = { 0, 40, 127, 200, 350, 1000, 31003, 52356, 23523522, 4324520398 };
            var stream = new MemoryStream();
            foreach (var t in sampleNumbers)
                stream.Write(t);

            var bytes = stream.ToArray();
            Assert.AreEqual(sampleNumbers.Length * 8, bytes.Length);

            var numberIndex = 0;
            for (var i = 0; i < bytes.Length; i += 8)
            {
                Assert.AreEqual((byte)(sampleNumbers[numberIndex] >> 56), bytes[i]);
                Assert.AreEqual((byte)(sampleNumbers[numberIndex] >> 48), bytes[i + 1]);
                Assert.AreEqual((byte)(sampleNumbers[numberIndex] >> 40), bytes[i + 2]);
                Assert.AreEqual((byte)(sampleNumbers[numberIndex] >> 32), bytes[i + 3]);
                Assert.AreEqual((byte)(sampleNumbers[numberIndex] >> 24), bytes[i + 4]);
                Assert.AreEqual((byte)(sampleNumbers[numberIndex] >> 16), bytes[i + 5]);
                Assert.AreEqual((byte)(sampleNumbers[numberIndex] >> 8), bytes[i + 6]);
                Assert.AreEqual((byte)(sampleNumbers[numberIndex] & 255), bytes[i + 7]);
                numberIndex++;
            }
        }

        [TestMethod]
        public void StreamExtension_TestWriteString()
        {
            var stream = new MemoryStream();
            stream.Write(null);

            var bytes = stream.ToArray();
            Assert.AreEqual(2, bytes.Length);
            Assert.AreEqual(0, bytes[0]);
            Assert.AreEqual(0, bytes[1]); ;

            stream = new MemoryStream();
            stream.Write("");

            bytes = stream.ToArray();
            Assert.AreEqual(2, bytes.Length);
            Assert.AreEqual(0, bytes[0]);
            Assert.AreEqual(0, bytes[1]);

            stream = new MemoryStream();
            stream.Write("ABCDE");

            bytes = stream.ToArray();
            Assert.AreEqual(12, bytes.Length);
            Assert.AreEqual(0, bytes[0]);
            Assert.AreEqual(10, bytes[1]);
            Assert.AreEqual(0, bytes[2]);
            Assert.AreEqual(65, bytes[3]);
            Assert.AreEqual(0, bytes[4]);
            Assert.AreEqual(66, bytes[5]);
            Assert.AreEqual(0, bytes[6]);
            Assert.AreEqual(67, bytes[7]);
            Assert.AreEqual(0, bytes[8]);
            Assert.AreEqual(68, bytes[9]);
        }

        [TestMethod]
        public void StreamExtension_TestReadByte()
        {
            var stream = new MemoryStream(new byte[] {});
            try
            {
                stream.ReadOneByte();
                Assert.Fail("Expected an exception");
            }
            catch
            {
                // ignore
            }

            stream = new MemoryStream(new byte[] {0, 1, 2, 3, 4 });
            var data = stream.ReadByte();
            Assert.AreEqual(0, data);
            data = stream.ReadByte();
            Assert.AreEqual(1, data);
            data = stream.ReadByte();
            Assert.AreEqual(2, data);
            data = stream.ReadByte();
            Assert.AreEqual(3, data);
            data = stream.ReadByte();
            Assert.AreEqual(4, data);

            try
            {
                stream.ReadOneByte();
                Assert.Fail("Expected an exception");
            }
            catch
            {
                // ignore
            }

        }

        [TestMethod]
        public void StreamExtension_TestReadShort()
        {
            var stream = new MemoryStream(new byte[] { });
            try
            {
                stream.ReadShort();
                Assert.Fail("Expected an exception");
            }
            catch
            {
                // ignore
            }

            stream = new MemoryStream(new byte[] { 1 });
            try
            {
                stream.ReadShort();
                Assert.Fail("Expected an exception");
            }
            catch
            {
                // ignore
            }

            stream = new MemoryStream(new byte[] { 0, 0, 0, 40, 0, 127, 1, 10 });
            var data = stream.ReadShort();
            Assert.AreEqual(0, data);
            data = stream.ReadShort();
            Assert.AreEqual(40, data);
            data = stream.ReadShort();
            Assert.AreEqual(127, data);
            data = stream.ReadShort();
            Assert.AreEqual(266, data);

            try
            {
                stream.ReadShort();
                Assert.Fail("Expected an exception");
            }
            catch
            {
                // ignore
            }
        }

        [TestMethod]
        public void StreamExtension_TestReadInt()
        {
            var stream = new MemoryStream(new byte[] { });
            try
            {
                stream.ReadInt();
                Assert.Fail("Expected an exception");
            }
            catch
            {
                // ignore
            }

            stream = new MemoryStream(new byte[] {  1, 2, 3 });
            try
            {
                stream.ReadInt();
                Assert.Fail("Expected an exception");
            }
            catch
            {
                // ignore
            }

            stream = new MemoryStream(new byte[] { 0, 0, 0, 40, 0, 0, 1, 10 });
            var data = stream.ReadInt();
            Assert.AreEqual(40, data);
            data = stream.ReadInt();
            Assert.AreEqual(266, data);

            try
            {
                stream.ReadInt();
                Assert.Fail("Expected an exception");
            }
            catch
            {
                // ignore
            }
        }

        [TestMethod]
        public void StreamExtension_TestReadLong()
        {
            var stream = new MemoryStream(new byte[] { });
            try
            {
                stream.ReadLong();
                Assert.Fail("Expected an exception");
            }
            catch
            {
                // ignore
            }

            stream = new MemoryStream(new byte[] { 0, 0, 0, 1, 2, 3 });
            try
            {
                stream.ReadLong();
                Assert.Fail("Expected an exception");
            }
            catch
            {
                // ignore
            }

            stream = new MemoryStream(new byte[] { 0, 0, 0, 0, 0, 0, 0, 40, 0, 0, 0, 0, 0, 0, 1, 10 });
            var data = stream.ReadLong();
            Assert.AreEqual(40, data);
            data = stream.ReadLong();
            Assert.AreEqual(266, data);

            try
            {
                stream.ReadLong();
                Assert.Fail("Expected an exception");
            }
            catch
            {
                // ignore
            }
        }

        [TestMethod]
        public void StreamExtension_TestReadString()
        {
            var stream = new MemoryStream(new byte[] { });
            try
            {
                stream.ReadString();
                Assert.Fail("Expected an exception");
            }
            catch
            {
                // ignore
            }

            stream = new MemoryStream(new byte[] { 0 });
            try
            {
                stream.ReadString();
                Assert.Fail("Expected an exception");
            }
            catch
            {
                // ignore
            }

            stream = new MemoryStream(new byte[] { 0, 0 });
            var data = stream.ReadString();
            Assert.AreEqual("", data);

            stream = new MemoryStream(new byte[] { 0, 6, 0, 65, 0, 66, 0, 67 });
            data = stream.ReadString();
            Assert.AreEqual("ABC", data);

            stream = new MemoryStream(new byte[] { 0, 6, 0, 65, 0, 66, 0 });
            try
            {
                stream.ReadString();
                Assert.Fail("Expected an exception");
            }
            catch
            {
                // ignore
            }

            stream = new MemoryStream(new byte[] { 0, 1, 65 });
            try
            {
                stream.ReadString();
                Assert.Fail("Expected an exception");
            }
            catch
            {
                // ignore
            }

        }

    }
}
