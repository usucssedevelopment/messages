﻿using System.IO;

namespace Messages
{
    public class BalanceReply : Reply
    {
        public long Balance { get; set; }

        protected override void InternalEncode(MemoryStream memoryStream)
        {
            base.InternalEncode(memoryStream);
            memoryStream.Write(Balance);
        }

        protected override void InternalDecode(MemoryStream memoryStream)
        {
            base.InternalDecode(memoryStream);
            Balance = memoryStream.ReadLong();
        }
    }
}
