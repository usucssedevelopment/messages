﻿using System.IO;

namespace Messages
{
    public class ErrorReply : Reply
    {
        public string Reason { get; set; }

        protected override void InternalEncode(MemoryStream memoryStream)
        {
            base.InternalEncode(memoryStream);
            if (Reason == null)
                Reason = string.Empty;

            Reason = Reason.Trim();

            memoryStream.Write(Reason);
        }

        protected override void InternalDecode(MemoryStream memoryStream)
        {
            base.InternalDecode(memoryStream);
            Reason = memoryStream.ReadString();
        }
    }
}
