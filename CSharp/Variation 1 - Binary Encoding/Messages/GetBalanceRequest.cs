﻿using System.IO;

namespace Messages
{
    public class GetBalanceRequest : Request
    {
        public string AccountNumber { get; set; }

        protected override void InternalEncode(MemoryStream memoryStream)
        {
            base.InternalEncode(memoryStream);
            if (AccountNumber == null)
                AccountNumber = string.Empty;

            AccountNumber = AccountNumber.Trim().ToLower();

            memoryStream.Write(AccountNumber);
        }

        protected override void InternalDecode(MemoryStream memoryStream)
        {
            base.InternalDecode(memoryStream);
            AccountNumber = memoryStream.ReadString();
        }
    }
}
