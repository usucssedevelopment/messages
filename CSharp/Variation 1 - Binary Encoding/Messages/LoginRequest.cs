﻿using System.IO;

namespace Messages
{
    public class LoginRequest : Message
    {
        public string Username { get; set; }
        public string Password { get; set; }

        protected override void InternalEncode(MemoryStream memoryStream)
        {
            base.InternalEncode(memoryStream);
            if (Username == null)
                Username = string.Empty;
            if (Password == null)
                Password = string.Empty;

            Username = Username.Trim().ToLower();
            Password = Password.Trim();

            memoryStream.Write(Username);
            memoryStream.Write(Password);
        }

        protected override void InternalDecode(MemoryStream memoryStream)
        {
            base.InternalDecode(memoryStream);
            Username = memoryStream.ReadString();
            Password = memoryStream.ReadString();
        }
    }
}
