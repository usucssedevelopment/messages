﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Messages
{
    public abstract class Message
    {
        private static readonly Dictionary<byte, Type> MessageTypes = new Dictionary<byte, Type>()
        {
            {1, typeof (Acknowledgement)},
            {2, typeof (BalanceReply)},
            {3, typeof (ErrorReply)},
            {4, typeof (GetBalanceRequest)},
            {5, typeof (LoginRequest)}
        };

        public int Id { get; set; }

        public byte[] Encode()
        {
            var memoryStream = new MemoryStream();
            InternalEncode(memoryStream);               // Call object's specific internal encode
            return memoryStream.ToArray();
        }

        public static Message Decode(byte[] bytes)
        {
            if (bytes==null || bytes.Length<1)
                throw new ApplicationException("Cannot decode message");

            var messageType = FindMessageType(bytes[0]);
            if (messageType == null)
                throw new ApplicationException("Unknown message type");

            var message = Activator.CreateInstance(messageType) as Message;
            if (message==null)
                throw new ApplicationException($"Cannot create a message of type {messageType}");

            var memoryStream = new MemoryStream(bytes);
            message.InternalDecode(memoryStream);

            return message;
        }

        protected virtual void InternalEncode(MemoryStream memoryStream)
        {
            memoryStream.Write(FindMessageTypeCode(this.GetType()));
            memoryStream.Write(Id);
        }

        protected virtual void InternalDecode(MemoryStream memoryStream)
        {
            memoryStream.ReadOneByte();            // Consume message type code
            Id = memoryStream.ReadInt();
        }

        private static byte FindMessageTypeCode(Type type)
        {
            return MessageTypes.Where(entry => entry.Value == type).Select(entry => entry.Key).FirstOrDefault();
        }

        private static Type FindMessageType(byte code)
        {
            return MessageTypes.ContainsKey(code) ? MessageTypes[code] : null;
        }
    }
}
