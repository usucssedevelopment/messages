﻿using System.IO;

namespace Messages
{
    public abstract class Reply : Message
    {
        public int RequestId { get; set; }

        protected override void InternalEncode(MemoryStream memoryStream)
        {
            base.InternalEncode(memoryStream);
            memoryStream.Write(RequestId);
        }

        protected override void InternalDecode(MemoryStream memoryStream)
        {
            base.InternalDecode(memoryStream);
            RequestId = memoryStream.ReadInt();
        }
    }
}
