﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Messages;

namespace MessageTest
{
    [TestClass]
    public class AcknowledgementTest
    {
        [TestMethod]
        public void Acknowledgement_Json_TestNormalMessage()
        {
            var serializers = new List<IMessageSerializer>() {new JsonMessageSerializer(), new XmlMessageSerializer()};
            foreach (var serializer in serializers)
            {
                var message = new Acknowledgement() { Id = 10, RequestId = 2 };

                var bytes = serializer.Encode(message);

                var message2 = serializer.Decode(bytes) as Acknowledgement;
                Assert.IsNotNull(message2);
                Assert.AreEqual(message.Id, message2.Id);
                Assert.AreEqual(message.RequestId, message2.RequestId);
            }
        }

        [TestMethod]
        public void Acknowledgement_Json_TestBadDecoding()
        {
            var serializers = new List<IMessageSerializer>() { new JsonMessageSerializer(), new XmlMessageSerializer() };
            foreach (var serializer in serializers)
            {
                var message = new Acknowledgement() {Id = 10, RequestId = 2};
                var bytes = serializer.Encode(message);
                TestUtils.CheckBadDecodings(serializer, bytes);
            }
        }


    }
}
