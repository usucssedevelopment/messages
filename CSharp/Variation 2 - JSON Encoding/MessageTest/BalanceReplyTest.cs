﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Messages;

namespace MessageTest
{
    [TestClass]
    public class BalanceReplyTest
    {
        [TestMethod]
        public void BalanceReply_TestNormalMessage()
        {
            var serializers = new List<IMessageSerializer>() { new JsonMessageSerializer(), new XmlMessageSerializer() };
            foreach (var serializer in serializers)
            {
                var message = new BalanceReply() {Id = 10, RequestId = 2, Balance = 17L};
                var bytes = serializer.Encode(message);

                var message2 = serializer.Decode(bytes) as BalanceReply;
                Assert.IsNotNull(message2);
                Assert.AreEqual(message.Id, message2.Id);
                Assert.AreEqual(message.RequestId, message2.RequestId);
                Assert.AreEqual(message.Balance, message2.Balance);
            }
        }

        [TestMethod]
        public void BalanceReply_TestBadDecoding()
        {
            var serializers = new List<IMessageSerializer>() { new JsonMessageSerializer(), new XmlMessageSerializer() };
            foreach (var serializer in serializers)
            {
                var message = new BalanceReply() {Id = 10, RequestId = 2, Balance = 17L};
                var bytes = serializer.Encode(message);
                TestUtils.CheckBadDecodings(serializer, bytes);
            }
        }

    }
}
