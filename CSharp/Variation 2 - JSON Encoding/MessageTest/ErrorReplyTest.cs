﻿using System.Collections.Generic;
using Messages;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MessageTest
{
    /// <summary>
    /// Summary description for ErrorReplyTest
    /// </summary>
    [TestClass]
    public class ErrorReplyTest
    {
        [TestMethod]
        public void ErrorReply_TestNormalMessage()
        {
            var serializers = new List<IMessageSerializer>() { new JsonMessageSerializer(), new XmlMessageSerializer() };
            foreach (var serializer in serializers)
            {
                var message = new ErrorReply() {Id = 10, RequestId = 2, Reason = "ABC"};
                var bytes = serializer.Encode(message);

                var message2 = serializer.Decode(bytes) as ErrorReply;
                Assert.IsNotNull(message2);
                Assert.AreEqual(message.Id, message2.Id);
                Assert.AreEqual(message.RequestId, message2.RequestId);
                Assert.AreEqual(message.Reason, message2.Reason);
            }
        }

        [TestMethod]
        public void ErrorReply_TestBlankReason()
        {
            var serializers = new List<IMessageSerializer>() { new JsonMessageSerializer(), new XmlMessageSerializer() };
            foreach (var serializer in serializers)
            {
                var message = new ErrorReply() {Id = 10, RequestId = 2, Reason = null};
                var bytes = serializer.Encode(message);

                var message2 = serializer.Decode(bytes) as ErrorReply;
                Assert.IsNotNull(message2);
                Assert.AreEqual(message.Id, message2.Id);
                Assert.AreEqual(message.RequestId, message2.RequestId);
                Assert.AreEqual(null, message2.Reason);
            }
        }

        [TestMethod]
        public void ErrorReply_TestBadDecoding()
        {
            var serializers = new List<IMessageSerializer>() { new JsonMessageSerializer(), new XmlMessageSerializer() };
            foreach (var serializer in serializers)
            {
                var message = new ErrorReply() { Id = 10, RequestId = 2, Reason = "ABC" };
                var bytes = serializer.Encode(message);
                TestUtils.CheckBadDecodings(serializer, bytes);
            }
        }
    }
}
