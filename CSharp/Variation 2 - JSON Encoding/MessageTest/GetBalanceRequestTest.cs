﻿using System;
using System.Text;
using System.Collections.Generic;
using Messages;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MessageTest
{
    /// <summary>
    /// Summary description for GetBalanceRequestTest
    /// </summary>
    [TestClass]
    public class GetBalanceRequestTest
    {
        [TestMethod]
        public void BalanceReply_TestNormalMessage()
        {
            var serializers = new List<IMessageSerializer>() { new JsonMessageSerializer(), new XmlMessageSerializer() };
            foreach (var serializer in serializers)
            {
                var message = new GetBalanceRequest() {Id = 2, AccountNumber = "A001"};
                var bytes = serializer.Encode(message);

                var message2 = serializer.Decode(bytes) as GetBalanceRequest;
                Assert.IsNotNull(message2);
                Assert.AreEqual(message.Id, message2.Id);
                Assert.AreEqual(message.AccountNumber.ToLower(), message2.AccountNumber);
            }
        }

        [TestMethod]
        public void BalanceReply_TestBlankAccountNumber()
        {
            var serializers = new List<IMessageSerializer>() { new JsonMessageSerializer(), new XmlMessageSerializer() };
            foreach (var serializer in serializers)
            {
                var message = new GetBalanceRequest() {Id = 2, AccountNumber = null};
                var bytes = serializer.Encode(message);

                var message2 = serializer.Decode(bytes) as GetBalanceRequest;
                Assert.IsNotNull(message2);
                Assert.AreEqual(message.Id, message2.Id);
                Assert.AreEqual(null, message2.AccountNumber);
            }
        }

        [TestMethod]
        public void BalanceReply_TestBadDecoding()
        {
            var serializers = new List<IMessageSerializer>() { new JsonMessageSerializer(), new XmlMessageSerializer() };
            foreach (var serializer in serializers)
            {
                var message = new GetBalanceRequest() { Id = 2, AccountNumber = "A001" };
                var bytes = serializer.Encode(message);
                TestUtils.CheckBadDecodings(serializer, bytes);
            }
        }

    }
}
