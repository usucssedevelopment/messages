﻿using System.Collections.Generic;
using Messages;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MessageTest
{
    [TestClass]
    public class LoginRequestTest
    {
        [TestMethod]
        public void LoginRequest_TestNormalMessage()
        {
            var serializers = new List<IMessageSerializer>() { new JsonMessageSerializer(), new XmlMessageSerializer() };
            foreach (var serializer in serializers)
            {
                var message = new LoginRequest() {Id = 2, Username = "Abc", Password = "AbC"};
                var bytes = serializer.Encode(message);

                var message2 = serializer.Decode(bytes) as LoginRequest;
                Assert.IsNotNull(message2);
                Assert.AreEqual(message.Id, message2.Id);
                Assert.AreEqual(message.Username, message2.Username);
                Assert.AreEqual(message.Password, message2.Password);
            }
        }

        [TestMethod]
        public void LoginRequest_TestBlankUsernameAndPassword()
        {
            var serializers = new List<IMessageSerializer>() { new JsonMessageSerializer(), new XmlMessageSerializer() };
            foreach (var serializer in serializers)
            {
                var message = new LoginRequest() {Id = 2, Username = null, Password = null};
                var bytes = serializer.Encode(message);

                var message2 = serializer.Decode(bytes) as LoginRequest;
                Assert.IsNotNull(message2);
                Assert.AreEqual(message.Id, message2.Id);
                Assert.AreEqual(null, message2.Username);
                Assert.AreEqual(null, message2.Password);
            }
        }

        [TestMethod]
        public void LoginRequest_TestBadDecoding()
        {
            var serializers = new List<IMessageSerializer>() { new JsonMessageSerializer(), new XmlMessageSerializer() };
            foreach (var serializer in serializers)
            {
                var message = new LoginRequest() {Id = 2, Username = "Abc", Password = "AAbbCC"};
                var bytes = serializer.Encode(message);
                TestUtils.CheckBadDecodings(serializer, bytes);
            }
        }

    }
}
