﻿using System;
using Messages;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MessageTest
{
    public static class TestUtils
    {
        public static void CheckBadDecodings(IMessageSerializer serializer, byte[] bytes)
        {
            for (var i = 0; i < bytes.Length - 1; i++)
            {
                var badBytes = new byte[i];
                Array.Copy(bytes, 0, badBytes, 0, i);

                try
                {
                    serializer.Decode(badBytes);
                    Assert.Fail("Expected an exception");
                }
                catch
                {
                    // ignore
                }
            }
        }
    }
}
