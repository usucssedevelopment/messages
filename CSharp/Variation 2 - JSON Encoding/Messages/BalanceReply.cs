﻿using System.Runtime.Serialization;

namespace Messages
{
    [DataContract]
    public class BalanceReply : Reply
    {
        [DataMember]
        public long Balance { get; set; }
    }
}
