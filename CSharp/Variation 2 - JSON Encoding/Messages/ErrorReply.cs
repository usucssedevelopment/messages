﻿using System.IO;
using System.Runtime.Serialization;

namespace Messages
{
    [DataContract]
    public class ErrorReply : Reply
    {
        [DataMember]
        public string Reason { get; set; }
    }
}
