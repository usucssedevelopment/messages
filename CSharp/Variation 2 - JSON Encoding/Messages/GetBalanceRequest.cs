﻿using System.IO;
using System.Runtime.Serialization;

namespace Messages
{
    [DataContract]
    public class GetBalanceRequest : Request
    {
        [DataMember]
        public string AccountNumber { get; set; }
    }
}
