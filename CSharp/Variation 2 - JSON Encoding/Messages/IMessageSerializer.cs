﻿namespace Messages
{
    public interface IMessageSerializer
    {
        byte[] Encode(Message message);
        Message Decode(byte[] bytes);
    }
}
