﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;

namespace Messages
{
    public class JsonMessageSerializer : IMessageSerializer
    {
        private static DataContractJsonSerializer _jsonSerializer;

        public JsonMessageSerializer()
        {
            if (_jsonSerializer==null)
                _jsonSerializer = new DataContractJsonSerializer(typeof (Message), Message.GetSubTypes());
        }

        public byte[] Encode(Message message)
        {
            if (message == null)
                throw new ApplicationException("Cannot encode a null message");

            var stream = new MemoryStream();
            _jsonSerializer.WriteObject(stream, message);
            return stream.ToArray();
        }

        public Message Decode(byte[] bytes)
        {
            if (bytes==null)
                throw new ApplicationException("Cannot decode an empty byte array");

            var stream = new MemoryStream(bytes);
            return _jsonSerializer.ReadObject(stream) as Message;
        }
    }
}
