﻿using System.IO;
using System.Runtime.Serialization;

namespace Messages
{
    [DataContract]
    public class LoginRequest : Message
    {
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
    }
}
