﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Messages
{
    [DataContract]
    public abstract class Message
    {
        private static readonly List<Type> SubTypes = new List<Type>()
        {
            typeof (Acknowledgement),
            typeof (BalanceReply),
            typeof (ErrorReply),
            typeof (GetBalanceRequest),
            typeof (LoginRequest),
            typeof (Request),
            typeof (Reply)
        };

        [DataMember]
        public int Id { get; set; }

        public static void AddSubType(Type subType)
        {
            SubTypes.Add(subType);
        }

        public static Type[] GetSubTypes()
        {
            return SubTypes.ToArray(); 
        }
    }
}
