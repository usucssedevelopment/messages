﻿using System.IO;
using System.Runtime.Serialization;

namespace Messages
{
    [DataContract]
    public abstract class Reply : Message
    {
        [DataMember]
        public int RequestId { get; set; }
    }
}
