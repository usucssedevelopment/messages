﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Messages
{
    public class XmlMessageSerializer : IMessageSerializer
    {
        private static XmlSerializer _xmlSerializer;

        public XmlMessageSerializer()
        {
            _xmlSerializer = new XmlSerializer(typeof(Message), Message.GetSubTypes());
        }

        public byte[] Encode(Message message)
        {
            if (message == null)
                throw new ApplicationException("Cannot encode a null message");

            var stream = new MemoryStream();
            _xmlSerializer.Serialize(stream, message);
            return stream.ToArray();
        }

        public Message Decode(byte[] bytes)
        {
            if (bytes == null)
                throw new ApplicationException("Cannot decode an empty byte array");

            var stream = new MemoryStream(bytes);
            return _xmlSerializer.Deserialize(stream) as Message;
        }
    }
}
