package edu.usu.csse.messages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.ByteBuffer;

public class BalanceReply extends Reply{
    private static Logger log = LogManager.getFormatterLogger(BalanceReply.class.getName());

    private long balance;
    public final long getBalance()

    {
        return balance;
    }
    public final void setBalance(long value)
    {
        balance = value;
    }

    @Override
    protected void InternalEncode(ByteBuffer byteBuffer)
    {
        super.InternalEncode(byteBuffer);
        ByteBufferExtension.write(byteBuffer, balance);
    }

    @Override
    protected void InternalDecode(ByteBuffer byteBuffer) throws Exception
    {
        super.InternalDecode(byteBuffer);
        balance = ByteBufferExtension.readLong(byteBuffer);

        log.debug("Decoded balance=%d", balance);
    }

}
