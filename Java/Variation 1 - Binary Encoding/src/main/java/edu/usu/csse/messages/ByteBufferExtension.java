package edu.usu.csse.messages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

public class ByteBufferExtension {
    private static Logger log = LogManager.getFormatterLogger(ByteBufferExtension.class.getName());

    public static void write(ByteBuffer byteBuffer, byte value)
    {
        byteBuffer.put(value);
    }

    public static void write(ByteBuffer byteBuffer, short value)
    {
        byteBuffer.order(ByteOrder.BIG_ENDIAN);
        byteBuffer.putShort(value);
    }

    public static void write(ByteBuffer byteBuffer, int value)
    {
        byteBuffer.order(ByteOrder.BIG_ENDIAN);
        byteBuffer.putInt(value);
    }


    public static void write(ByteBuffer byteBuffer, long value)
    {
        byteBuffer.order(ByteOrder.BIG_ENDIAN);
        byteBuffer.putLong(value);
    }

    public static void write(ByteBuffer byteBuffer, String value)
    {
        if (value == null)
            value = "";

        byte[] textBytes = value.getBytes(Charset.forName("UTF-16BE"));
        write(byteBuffer, (short) textBytes.length);
        byteBuffer.put(textBytes);
    }

    public static byte readByte(ByteBuffer byteBuffer)
    {
        return byteBuffer.get();
    }

    public static short readShort(ByteBuffer byteBuffer)
    {
        byteBuffer.order(ByteOrder.BIG_ENDIAN);
        return byteBuffer.getShort();
    }

    public static int readInt(ByteBuffer byteBuffer)
    {
        byteBuffer.order(ByteOrder.BIG_ENDIAN);
        return byteBuffer.getInt();
    }

    public static long readLong(ByteBuffer byteBuffer)
    {
        byteBuffer.order(ByteOrder.BIG_ENDIAN);
        return byteBuffer.getLong();
    }

    public static String readString(ByteBuffer byteBuffer) throws Exception {
        short textLength = readShort(byteBuffer);
        if ((textLength % 2) != 0) {
            log.warn("Invalid specified text length");
            throw new Exception("Invalid specified text length");
        }

        log.debug("In readString, textLength=%d remaining bytes=%d", textLength, byteBuffer.remaining() );
        if (byteBuffer.remaining() < textLength) {
            log.warn("Byte array is too short for pecified text length");
            throw new Exception("Byte array is too short for specified text length");
        }


        byte[] textBytes = new byte[textLength];
        byteBuffer.get(textBytes, 0, textLength);
        return new String(textBytes, Charset.forName("UTF-16BE"));
    }
}
