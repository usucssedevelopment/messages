package edu.usu.csse.messages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.ByteBuffer;

public class ErrorReply extends Reply{
    private static Logger log = LogManager.getFormatterLogger(ErrorReply.class.getName());

    private String reason;
    public  String getReason()
    {
        return this.reason;
    }
    public void setReason(String value)
    {
        this.reason = value;
    }

    @Override
    protected void InternalEncode(ByteBuffer byteBuffer)
    {
        super.InternalEncode(byteBuffer);
        if (reason == null)
            reason = "";

        ByteBufferExtension.write(byteBuffer, reason);
    }

    @Override
    protected void InternalDecode(ByteBuffer byteBuffer) throws Exception
    {
        super.InternalDecode(byteBuffer);
        reason = ByteBufferExtension.readString(byteBuffer);
    }
}
