package edu.usu.csse.messages;

import com.sun.accessibility.internal.resources.accessibility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.ByteBuffer;

public class GetBalanceRequest extends Request{
    private static Logger log = LogManager.getFormatterLogger(GetBalanceRequest.class.getName());

    private String accountNumber;
    public  String getAccountNumber()
    {
        return this.accountNumber;
    }
    public void setAccountNumber(String value)
    {
        this.accountNumber = value;
    }

    @Override
    protected void InternalEncode(ByteBuffer byteBuffer)
    {
        super.InternalEncode(byteBuffer);
        if (accountNumber == null)
            accountNumber = "";

        ByteBufferExtension.write(byteBuffer, accountNumber);
    }

    @Override
    protected void InternalDecode(ByteBuffer byteBuffer) throws Exception {
        super.InternalDecode(byteBuffer);
        accountNumber = ByteBufferExtension.readString(byteBuffer);

        log.debug("Decoded accountNumber=%s", accountNumber);
    }
}
