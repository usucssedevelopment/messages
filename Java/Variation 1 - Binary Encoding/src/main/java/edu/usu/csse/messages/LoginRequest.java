package edu.usu.csse.messages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.ByteBuffer;

public class LoginRequest extends Message {
    private static Logger log = LogManager.getFormatterLogger(LoginRequest.class.getName());

    private String username;
    public  String getUsername()
    {
        return this.username;
    }
    public  void setUsername(String value)
    {
        this.username = value;
    }
    private String password;
    public  String getPassword()
    {
        return password;
    }
    public  void setPassword(String value)
    {
        password = value;
    }

    @Override
    protected void InternalEncode(ByteBuffer byteBuffer)
    {
        super.InternalEncode(byteBuffer);
        if (username == null)
            username = "";

        if (password == null)
            password = "";

        ByteBufferExtension.write(byteBuffer, username.trim().toLowerCase());
        ByteBufferExtension.write(byteBuffer, password.trim());
    }

    @Override
    protected void InternalDecode(ByteBuffer byteBuffer) throws Exception
    {
        super.InternalDecode(byteBuffer);

        username = ByteBufferExtension.readString(byteBuffer);
        log.debug("Decoded username = %s", username);

        password = ByteBufferExtension.readString(byteBuffer);
        log.debug("Decoded password = %s", password);
    }
}
