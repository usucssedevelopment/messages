package edu.usu.csse.messages;
import java.nio.ByteBuffer;
import java.util.*;
import java.lang.Exception;
import java.util.HashMap;

public abstract class Message {
    private static final HashMap<Byte, java.lang.Class> MessageTypes = new java.util.HashMap<Byte, Class>();
    static
    {
        MessageTypes.put((byte) 1, Acknowledgement.class);
        MessageTypes.put((byte) 2, BalanceReply.class);
        MessageTypes.put((byte) 3, ErrorReply.class);
        MessageTypes.put((byte) 4, GetBalanceRequest.class);
        MessageTypes.put((byte) 5, LoginRequest.class);
    }

    private int id;
    public  int getId()
    {
        return this.id;
    }
    public  void setId(int value)
    {
        this.id = value;
    }

    public final byte[] encode()
    {
        ByteBuffer byteBuffer = ByteBuffer.allocate(4096);
        InternalEncode(byteBuffer);
        int count = byteBuffer.position();
        byte[] result = new byte[count];
        System.arraycopy(byteBuffer.array(), 0, result, 0, count);
        return result;
    }

    public static Message decode(byte[] bytes) throws Exception
    {
        if (bytes==null || bytes.length<1)
            throw new Exception("Cannot decode message");

        Class messageType = findMessageType(bytes[0]);
        if (messageType == null)
            throw new Exception("Unknown message type");

        Message message = (Message) messageType.newInstance();
        if (message==null)
            throw new Exception("Cannot create a message of type {messageType}");

        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        message.InternalDecode(byteBuffer);

        return message;
    }

    protected void InternalEncode(ByteBuffer byteBuffer)
    {
        ByteBufferExtension.write(byteBuffer, FindMessageTypeCode(this.getClass()));
        ByteBufferExtension.write(byteBuffer, id);
    }

    protected void InternalDecode(ByteBuffer byteBuffer) throws Exception
    {
        ByteBufferExtension.readByte(byteBuffer);           // Consume the message type byte
        setId(ByteBufferExtension.readInt(byteBuffer));
    }

    private static Byte FindMessageTypeCode(Class type)
    {
        Byte messageTypeCode=null;
        for (Map.Entry<Byte, Class> entry : MessageTypes.entrySet()) {
            if (entry.getValue().equals(type)) {
                messageTypeCode = entry.getKey();
                break;
            }
        }
        return messageTypeCode;
    }

    private static Class findMessageType(Byte code)
    {
        if (MessageTypes.containsKey(code))
            return MessageTypes.get(code);
        return null;
    }

}
