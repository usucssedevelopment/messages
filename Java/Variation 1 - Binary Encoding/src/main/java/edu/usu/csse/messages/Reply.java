package edu.usu.csse.messages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.ByteBuffer;

public abstract class Reply extends Message{
    private static Logger log = LogManager.getFormatterLogger(LoginRequest.class.getName());
    private int requestId;

    public int getRequestId() { return requestId; }
    public void setRequestId(int requestId) { this.requestId = requestId; }

    @Override
    protected void InternalEncode(ByteBuffer byteBuffer)
    {
        super.InternalEncode(byteBuffer);
        ByteBufferExtension.write(byteBuffer, requestId);
    }

    @Override
    protected void InternalDecode(ByteBuffer byteBuffer) throws Exception {
        super.InternalDecode(byteBuffer);
        requestId = ByteBufferExtension.readInt(byteBuffer);
        log.debug("Decoded requestId = %d", requestId);
    }
}
