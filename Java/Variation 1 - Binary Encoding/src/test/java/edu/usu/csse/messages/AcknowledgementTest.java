package edu.usu.csse.messages;

import org.junit.Test;

import static org.junit.Assert.*;

public class AcknowledgementTest {
    
    @Test
    public void testNormalMessage() throws Exception
    {
        Acknowledgement message = new Acknowledgement();
        message.setId(10);
        message.setRequestId(2);
        assertEquals(10, message.getId());
        byte[] bytes = message.encode();

        assertEquals(9, bytes.length);
        assertEquals(1, bytes[0]);
        assertEquals(0, bytes[1]);
        assertEquals(0, bytes[2]);
        assertEquals(0, bytes[3]);
        assertEquals(10, bytes[4]);
        assertEquals(0, bytes[5]);
        assertEquals(0, bytes[6]);
        assertEquals(0, bytes[7]);
        assertEquals(2, bytes[8]);

        Acknowledgement message2 = (Acknowledgement) Message.decode(bytes);
        assertNotNull(message2);
        assertEquals(message.getId(), message2.getId());
        assertEquals(message.getRequestId(), message2.getRequestId());
    }

    @Test
    public void testBadDecoding()
    {
        Acknowledgement message = new Acknowledgement();
        message.setId(10);
        message.setRequestId(2);

        byte[] bytes = message.encode();
        TestUtils.checkBadDecodings(bytes);
    }
    

}