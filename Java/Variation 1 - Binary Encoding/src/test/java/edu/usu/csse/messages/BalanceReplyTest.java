package edu.usu.csse.messages;

import org.junit.Test;

import static org.junit.Assert.*;

public class BalanceReplyTest {
    
    @Test
    public void testNormalMessage() throws Exception
    {
        BalanceReply message = new BalanceReply();
        message.setId(10);
        message.setRequestId(2);
        message.setBalance(17L);
        byte[] bytes = message.encode();

        assertEquals(17, bytes.length);
        assertEquals(2, bytes[0]);
        assertEquals(0, bytes[1]);
        assertEquals(0, bytes[2]);
        assertEquals(0, bytes[3]);
        assertEquals(10, bytes[4]);
        assertEquals(0, bytes[5]);
        assertEquals(0, bytes[6]);
        assertEquals(0, bytes[7]);
        assertEquals(2, bytes[8]);
        assertEquals(0, bytes[9]);
        assertEquals(0, bytes[10]);
        assertEquals(0, bytes[11]);
        assertEquals(0, bytes[12]);
        assertEquals(0, bytes[13]);
        assertEquals(0, bytes[14]);
        assertEquals(0, bytes[15]);
        assertEquals(17, bytes[16]);

        BalanceReply message2 = (BalanceReply) Message.decode(bytes);
        assertEquals(message.getId(), message2.getId());
        assertEquals(message.getRequestId(), message2.getRequestId());
        assertEquals(message.getBalance(), message2.getBalance());
    }

    @Test
    public void testBadDecoding()
    {
        BalanceReply message = new BalanceReply();
        message.setId(10);
        message.setRequestId(2);
        message.setBalance(17L);
        byte[] bytes = message.encode();
        TestUtils.checkBadDecodings(bytes);
    }

}