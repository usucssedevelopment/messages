package edu.usu.csse.messages;

import org.junit.Test;

import java.nio.ByteBuffer;

import static org.junit.Assert.*;

public class ByteBufferExtensionTest {
    
    @Test
    public void testWriteByte()
    {
        ByteBuffer byteBuffer = ByteBuffer.allocate(100);
        for (byte i=0; i<100; i++)
            ByteBufferExtension.write(byteBuffer, i);

        byte[] bytes = byteBuffer.array();
        assertEquals(100, bytes.length);
        for (byte i= 0; i<100; i++)
            assertEquals(i, bytes[i]);
    }

    @Test
    public void testWriteShort()
    {
        short[] sampleNumbers = {0, 40, 127, 200, 350, 1000, 31003};
        ByteBuffer byteBuffer = ByteBuffer.allocate(sampleNumbers.length*2);
        for (short t : sampleNumbers)
            ByteBufferExtension.write(byteBuffer, t);

        byte[] bytes = byteBuffer.array();
        assertEquals(sampleNumbers.length*2, bytes.length);

        int numberIndex = 0;
        for (int i = 0; i < bytes.length; i += 2)
        {
            assertEquals((byte)(sampleNumbers[numberIndex] >> 8), bytes[i]);
            assertEquals((byte)(sampleNumbers[numberIndex] & 255), bytes[i + 1]);
            numberIndex++;
        }
    }

    @Test
    public void testWriteInt()
    {
        int[] sampleNumbers = { 0, 40, 127, 200, 350, 1000, 31003, 52356, 23523522 };
        ByteBuffer byteBuffer = ByteBuffer.allocate(sampleNumbers.length*4);
        for (int t : sampleNumbers)
            ByteBufferExtension.write(byteBuffer, t);

        byte[] bytes = byteBuffer.array();
        assertEquals(sampleNumbers.length*4, sampleNumbers.length*4);

        int numberIndex = 0;
        for (int i = 0; i < bytes.length; i += 4)
        {
            assertEquals((byte)(sampleNumbers[numberIndex] >> 24), bytes[i]);
            assertEquals((byte)(sampleNumbers[numberIndex] >> 16), bytes[i + 1]);
            assertEquals((byte)(sampleNumbers[numberIndex] >> 8), bytes[i + 2]);
            assertEquals((byte)(sampleNumbers[numberIndex] & 255), bytes[i + 3]);
            numberIndex++;
        }
    }

    @Test
    public void testWriteLong()
    {
        long[] sampleNumbers = { 0L, 40L, 127L, 200L, 350L, 1000L, 31003L, 52356L, 23523522L, 4324520398L };
        ByteBuffer byteBuffer = ByteBuffer.allocate(sampleNumbers.length*8);
        for (long t : sampleNumbers)
            ByteBufferExtension.write(byteBuffer, t);

        byte[] bytes = byteBuffer.array();
        assertEquals(sampleNumbers.length*8, sampleNumbers.length*8);

        int numberIndex = 0;
        for (int i = 0; i < bytes.length; i += 8)
        {
            assertEquals((byte)(sampleNumbers[numberIndex] >> 56), bytes[i]);
            assertEquals((byte)(sampleNumbers[numberIndex] >> 48), bytes[i + 1]);
            assertEquals((byte)(sampleNumbers[numberIndex] >> 40), bytes[i + 2]);
            assertEquals((byte)(sampleNumbers[numberIndex] >> 32), bytes[i + 3]);
            assertEquals((byte)(sampleNumbers[numberIndex] >> 24), bytes[i + 4]);
            assertEquals((byte)(sampleNumbers[numberIndex] >> 16), bytes[i + 5]);
            assertEquals((byte)(sampleNumbers[numberIndex] >> 8), bytes[i + 6]);
            assertEquals((byte)(sampleNumbers[numberIndex] & 255), bytes[i + 7]);
            numberIndex++;
        }
    }

    @Test
    public void testWriteString()
    {
        ByteBuffer byteBuffer = ByteBuffer.allocate(2);
        ByteBufferExtension.write(byteBuffer, null);

        byte[] bytes = byteBuffer.array();
        assertEquals(2, bytes.length);
        assertEquals(0, bytes[0]);
        assertEquals(0, bytes[1]);

        byteBuffer = ByteBuffer.allocate(2);
        ByteBufferExtension.write(byteBuffer, "");

        bytes = byteBuffer.array();
        assertEquals(2, bytes.length);
        assertEquals(0, bytes[0]);
        assertEquals(0, bytes[1]);

        byteBuffer = ByteBuffer.allocate(12);
        ByteBufferExtension.write(byteBuffer, "ABCDE");

        bytes = byteBuffer.array();
        assertEquals(12, bytes.length);
        assertEquals(0, bytes[0]);
        assertEquals(10, bytes[1]);
        assertEquals(0, bytes[2]);
        assertEquals(65, bytes[3]);
        assertEquals(0, bytes[4]);
        assertEquals(66, bytes[5]);
        assertEquals(0, bytes[6]);
        assertEquals(67, bytes[7]);
        assertEquals(0, bytes[8]);
        assertEquals(68, bytes[9]);
    }

    @Test
    public void testReadByte()
    {
        ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[] {});
        try
        {
            ByteBufferExtension.readByte(byteBuffer);
            fail("Expected an exception");
        }
        catch (Exception e)
        {
            // ignore
        }

        byteBuffer = ByteBuffer.wrap(new byte[] {0, 1, 2, 3, 4});
        byte data = ByteBufferExtension.readByte(byteBuffer);
        assertEquals(0, data);
        data = ByteBufferExtension.readByte(byteBuffer);
        assertEquals(1, data);
        data = ByteBufferExtension.readByte(byteBuffer);
        assertEquals(2, data);
        data = ByteBufferExtension.readByte(byteBuffer);
        assertEquals(3, data);
        data = ByteBufferExtension.readByte(byteBuffer);
        assertEquals(4, data);

        try
        {
            ByteBufferExtension.readByte(byteBuffer);
            fail("Expected an exception");
        }
        catch (Exception e)
        {
            // ignore
        }

    }

    @Test
    public void testReadShort()
    {
        ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[] {});
        try
        {
            ByteBufferExtension.readShort(byteBuffer);
            fail("Expected an exception");
        }
        catch (Exception e)
        {
            // ignore
        }

        byteBuffer = ByteBuffer.wrap(new byte[] { 1 });
        try
        {
            ByteBufferExtension.readShort(byteBuffer);
            fail("Expected an exception");
        }
        catch (Exception e)
        {
            // ignore
        }

        byteBuffer = ByteBuffer.wrap(new byte[] { 0, 0, 0, 40, 0, 127, 1, 10  });
        short data = ByteBufferExtension.readShort(byteBuffer);
        assertEquals(0, data);
        data = ByteBufferExtension.readShort(byteBuffer);
        assertEquals(40, data);
        data = ByteBufferExtension.readShort(byteBuffer);
        assertEquals(127, data);
        data = ByteBufferExtension.readShort(byteBuffer);
        assertEquals(266, data);

        try
        {
            ByteBufferExtension.readShort(byteBuffer);
            fail("Expected an exception");
        }
        catch (Exception e)
        {
            // ignore
        }
    }

    @Test
    public void testReadInt()
    {
        ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[] {});
        try
        {
            ByteBufferExtension.readInt(byteBuffer);
            fail("Expected an exception");
        }
        catch (Exception e)
        {
            // ignore
        }

        byteBuffer = ByteBuffer.wrap(new byte[] { 1, 2, 3 });
        try
        {
            ByteBufferExtension.readInt(byteBuffer);
            fail("Expected an exception");
        }
        catch (Exception e)
        {
            // ignore
        }

        byteBuffer = ByteBuffer.wrap(new byte[] { 0, 0, 0, 40, 0, 0, 1, 10 });
        int data = ByteBufferExtension.readInt(byteBuffer);
        assertEquals(40, data);
        data = ByteBufferExtension.readInt(byteBuffer);
        assertEquals(266, data);

        try
        {
            ByteBufferExtension.readInt(byteBuffer);
            fail("Expected an exception");
        }
        catch (Exception e)
        {
            // ignore
        }
    }

    @Test
    public void testReadLong()
    {
        ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[] {});
        try
        {
            ByteBufferExtension.readLong(byteBuffer);
            fail("Expected an exception");
        }
        catch (Exception e)
        {
            // ignore
        }

        byteBuffer = ByteBuffer.wrap(new byte[] { 1, 2, 3, 4, 5, 6, 7 });
        try
        {
            ByteBufferExtension.readLong(byteBuffer);
            fail("Expected an exception");
        }
        catch (Exception e)
        {
            // ignore
        }

        byteBuffer = ByteBuffer.wrap(new byte[] { 0, 0, 0, 0, 0, 0, 0, 40, 0, 0, 0, 0, 0, 0, 1, 10 });
        long data = ByteBufferExtension.readLong(byteBuffer);
        assertEquals(40, data);
        data = ByteBufferExtension.readLong(byteBuffer);
        assertEquals(266, data);

        try
        {
            ByteBufferExtension.readLong(byteBuffer);
            fail("Expected an exception");
        }
        catch (Exception e)
        {
            // ignore
        }
    }

    @Test
    public void testReadString() throws Exception
    {
        ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[] {});
        try
        {
            ByteBufferExtension.readString(byteBuffer);
            fail("Expected an exception");
        }
        catch (Exception e)
        {
            // ignore
        }

        byteBuffer = ByteBuffer.wrap(new byte[] { 0 });
        try
        {
            ByteBufferExtension.readString(byteBuffer);
            fail("Expected an exception");
        }
        catch (Exception e)
        {
            // ignore
        }

        byteBuffer = ByteBuffer.wrap(new byte[] { 0, 0 });
        String data = ByteBufferExtension.readString(byteBuffer);
        assertEquals("", data);

        byteBuffer = ByteBuffer.wrap(new byte[] { 0, 6, 0, 65, 0, 66, 0, 67 });
        data = ByteBufferExtension.readString(byteBuffer);
        assertEquals("ABC", data);

        byteBuffer = ByteBuffer.wrap(new byte[] { 0, 6, 0, 65, 0, 66, 0 });
        try
        {
            ByteBufferExtension.readString(byteBuffer);
            fail("Expected an exception");
        }
        catch (Exception e)
        {
            // ignore
        }

        byteBuffer = ByteBuffer.wrap(new byte[] { 0, 1, 65 });
        try
        {
            ByteBufferExtension.readString(byteBuffer);
            fail("Expected an exception");
        }
        catch (Exception e)
        {
            // ignore
        }
    }
    

}