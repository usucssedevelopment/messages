package edu.usu.csse.messages;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ErrorReplyTest {

    @Test
    public void testNormalMessage() throws Exception
    {
        ErrorReply message = new ErrorReply();
        message.setId(10);
        message.setRequestId(2);
        message.setReason("ABC");
        byte[] bytes = message.encode();

        assertEquals(17, bytes.length);
        assertEquals(3, bytes[0]);
        assertEquals(0, bytes[1]);
        assertEquals(0, bytes[2]);
        assertEquals(0, bytes[3]);
        assertEquals(10, bytes[4]);
        assertEquals(0, bytes[5]);
        assertEquals(0, bytes[6]);
        assertEquals(0, bytes[7]);
        assertEquals(2, bytes[8]);
        assertEquals(0, bytes[9]);
        assertEquals(6, bytes[10]);
        assertEquals(0, bytes[11]);
        assertEquals(65, bytes[12]);
        assertEquals(0, bytes[13]);
        assertEquals(66, bytes[14]);
        assertEquals(0, bytes[15]);
        assertEquals(67, bytes[16]);

        ErrorReply message2 = (ErrorReply) Message.decode(bytes);
        assertEquals(message.getId(), message2.getId());
        assertEquals(message.getRequestId(), message2.getRequestId());
        assertEquals(message.getReason(), message2.getReason());
    }

    @Test
    public void testBlankReason() throws Exception
    {
        ErrorReply message = new ErrorReply();
        message.setId(10);
        message.setRequestId(2);
        message.setReason(null);
        byte[] bytes = message.encode();

        assertEquals(11, bytes.length);
        assertEquals(3, bytes[0]);
        assertEquals(0, bytes[1]);
        assertEquals(0, bytes[2]);
        assertEquals(0, bytes[3]);
        assertEquals(10, bytes[4]);
        assertEquals(0, bytes[5]);
        assertEquals(0, bytes[6]);
        assertEquals(0, bytes[7]);
        assertEquals(2, bytes[8]);
        assertEquals(0, bytes[9]);
        assertEquals(0, bytes[10]);

        ErrorReply message2 = (ErrorReply) Message.decode(bytes);
        assertEquals(message.getId(), message2.getId());
        assertEquals(message.getRequestId(), message2.getRequestId());
        assertEquals(message.getReason(), message2.getReason());
    }

    @Test
    public void testBadDecoding() throws Exception
    {
        ErrorReply message = new ErrorReply();
        message.setId(10);
        message.setRequestId(2);
        message.setReason("ABC");
        byte[] bytes = message.encode();

        TestUtils.checkBadDecodings(bytes);
    }
}