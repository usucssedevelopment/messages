package edu.usu.csse.messages;

import org.junit.Test;

import static org.junit.Assert.*;

public class GetBalanceRequestTest {

    @Test
    public void testNormalMessage() throws Exception
    {
        GetBalanceRequest message = new GetBalanceRequest();
        message.setId(10);
        message.setAccountNumber("A001");
        byte[] bytes = message.encode();

        assertEquals(15, bytes.length);
        assertEquals(4, bytes[0]);
        assertEquals(0, bytes[1]);
        assertEquals(0, bytes[2]);
        assertEquals(0, bytes[3]);
        assertEquals(10, bytes[4]);
        assertEquals(0, bytes[5]);
        assertEquals(8, bytes[6]);
        assertEquals(0, bytes[7]);
        assertEquals(65, bytes[8]);
        assertEquals(0, bytes[9]);
        assertEquals(48, bytes[10]);
        assertEquals(0, bytes[11]);
        assertEquals(48, bytes[12]);
        assertEquals(0, bytes[13]);
        assertEquals(49, bytes[14]);

        GetBalanceRequest message2 = (GetBalanceRequest) Message.decode(bytes);
        assertEquals(message.getId(), message2.getId());
        assertEquals(message.getAccountNumber(), message2.getAccountNumber());
    }

    @Test
    public void testBlankAccountNumber() throws Exception
    {
        GetBalanceRequest message = new GetBalanceRequest();
        message.setId(10);
        message.setAccountNumber(null);
        byte[] bytes = message.encode();

        assertEquals(7, bytes.length);
        assertEquals(4, bytes[0]);
        assertEquals(0, bytes[1]);
        assertEquals(0, bytes[2]);
        assertEquals(0, bytes[3]);
        assertEquals(10, bytes[4]);
        assertEquals(0, bytes[5]);
        assertEquals(0, bytes[6]);

        GetBalanceRequest message2 = (GetBalanceRequest) Message.decode(bytes);
        assertEquals(message.getId(), message2.getId());
        assertEquals(message.getAccountNumber().toLowerCase(), message2.getAccountNumber());

    }

    @Test
    public void testBadDecoding()
    {
        GetBalanceRequest message = new GetBalanceRequest();
        message.setId(10);
        message.setAccountNumber(null);
        byte[] bytes = message.encode();

        TestUtils.checkBadDecodings(bytes);
    }
}