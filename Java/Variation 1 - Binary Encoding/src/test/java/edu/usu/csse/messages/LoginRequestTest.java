package edu.usu.csse.messages;

import org.junit.Test;

import static org.junit.Assert.*;

public class LoginRequestTest {

    @Test
    public void testNormalMessage() throws Exception
    {
        byte[] expectedBytes = new byte[] {5, 0, 0, 0, 2, 0, 6, 0, 97, 0, 98, 0, 99, 0, 6, 0, 65, 0, 98, 0, 67};
        LoginRequest message = new LoginRequest();
        message.setId(2);
        message.setUsername(" Abc   ");
        message.setPassword("   AbC ");
        byte[] bytes = message.encode();

        assertArrayEquals(expectedBytes, bytes);

        LoginRequest message2 = (LoginRequest) Message.decode(bytes);
        assertEquals(message.getId(), message2.getId());
        assertEquals(message.getUsername().trim().toLowerCase(), message2.getUsername());
        assertEquals(message.getPassword().trim(), message2.getPassword());
    }

    @Test
    public void testBlankUsernameAndPassword() throws Exception
    {
        byte[] expectedBytes = new byte[] { 5, 0, 0, 0, 2, 0, 0, 0, 0 };
        LoginRequest message = new LoginRequest();
        message.setId(2);
        message.setUsername(null);
        message.setPassword(null);
        byte[] bytes = message.encode();

        assertArrayEquals(expectedBytes, bytes);

        LoginRequest message2 = (LoginRequest) Message.decode(bytes);
        assertEquals(message.getId(), message2.getId());
        assertEquals(message.getUsername().trim().toLowerCase(), message2.getUsername());
        assertEquals(message.getPassword().trim(), message2.getPassword());
    }

    @Test
    public void testBadDecoding()
    {
        LoginRequest message = new LoginRequest();
        message.setId(2);
        message.setUsername(" Abc   ");
        message.setPassword("   AbC ");
        byte[] bytes = message.encode();

        TestUtils.checkBadDecodings(bytes);
    }
}