package edu.usu.csse.messages;

import static junit.framework.TestCase.fail;

class TestUtils {

    static void checkBadDecodings(byte[] bytes)
    {
        for (int i = 0; i < bytes.length - 1; i++)
        {
            byte[] badBytes = new byte[i];
            System.arraycopy(bytes, 0, badBytes, 0, i);

            try
            {
                Message.decode(badBytes);
                fail("Expected an exception");
            }
            catch (Exception e)
            {
                // ignore
            }
        }
    }
}
