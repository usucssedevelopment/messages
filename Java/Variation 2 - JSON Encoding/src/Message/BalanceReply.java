﻿package Message;


//[DataContract]
public class BalanceReply extends Reply
{

	private long privateBalance;
	public  long getBalance()
	{
		return this.privateBalance;
	}
	public  void setBalance(long value)
	{
		this.privateBalance = value;
	}
}