﻿package Message;


public class ErrorReply extends Reply
{

	private String privateReason;
	public final String getReason()
	{
		return this.privateReason;
	}
	public final void setReason(String value)
	{
		this.privateReason = value;
	}
}