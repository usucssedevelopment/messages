﻿package Message;


public class GetBalanceRequest extends Request
{

	private String privateAccountNumber;
	public final String getAccountNumber()
	{
		return this.privateAccountNumber;
	}
	public final void setAccountNumber(String value)
	{
		this.privateAccountNumber = value;
	}
}