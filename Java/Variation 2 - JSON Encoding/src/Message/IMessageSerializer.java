﻿package Message;


public interface IMessageSerializer
{
	byte[] Encode(Message message) throws Exception;
	Message Decode(byte[] bytes) throws Exception;
}