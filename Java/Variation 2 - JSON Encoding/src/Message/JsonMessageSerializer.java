﻿package Message;
import  java.lang.Exception;
import java.nio.ByteBuffer;


import net.sf.json.JSON;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;


public class JsonMessageSerializer implements IMessageSerializer
{
	private static JSONSerializer _jsonSerializer=new JSONSerializer();

	public static JSONSerializer get_jsonSerializer() {
		return _jsonSerializer;
	}

	public static void set_jsonSerializer(JSONSerializer _jsonSerializer) {
		JsonMessageSerializer._jsonSerializer = _jsonSerializer;
	}

	public final byte[] Encode(Message message) throws JSONException
	{
		if (message == null)
		{
			throw new JSONException("Cannot encode a null message");
		}



        JSON jsonObject=_jsonSerializer.toJSON(message);
		return jsonObject.toString().getBytes();

	}

	public final Message Decode(byte[] bytes) throws Exception
	{
		if (bytes==null)
		{
			throw new Exception("Cannot decode an empty byte array");
		}

		String str=bytes.toString();
		JSONObject jsonObject=JSONObject.fromObject(str);


		return (Message)(_jsonSerializer.toJava(jsonObject));

	}
}