﻿package Message;


public class LoginRequest extends Message{

	private String privateUsername;
	public final String getUsername()
	{
		return privateUsername;
	}
	public final void setUsername(String value)
	{
		privateUsername = value;
	}

	private String privatePassword;
	public final String getPassword()
	{
		return privatePassword;
	}
	public final void setPassword(String value)
	{
		privatePassword = value;
	}
}