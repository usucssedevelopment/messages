﻿package Message;
import  java.lang.Class;
import java.util.ArrayList;
import java.util.Arrays;



public abstract class Message
{
	private static final ArrayList<Class> SubTypes = new ArrayList<java.lang.Class>
			(Arrays.asList((new Class[]{Acknowledgement.class,
					            BalanceReply.class,
					            ErrorReply.class,
					            GetBalanceRequest.class,
					            LoginRequest.class,
					            Request.class,
					            Reply.class})));


	private int privateId;
	public final int getId()
	{
		return this.privateId;
	}
	public final void setId(int value)
	{
		this.privateId = value;
	}

	public static void addSubType(Class subType)
	{
		SubTypes.add(subType);
	}

	public static Class[] getSubTypes()
	{
		return SubTypes.toArray(new java.lang.Class[]{});
	}
}