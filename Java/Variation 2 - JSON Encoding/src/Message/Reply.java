﻿package Message;



public abstract class Reply extends Message
{


	private int privateRequestId;
	public  int getRequestId()
	{
		return this.privateRequestId;
	}
	public final void setRequestId(int value)
	{
		this.privateRequestId = value;
	}
}