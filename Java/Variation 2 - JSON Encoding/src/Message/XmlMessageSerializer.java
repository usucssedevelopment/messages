﻿package Message;



import com.thoughtworks.xstream.XStream;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.Exception;
import java.nio.ByteBuffer;

public class XmlMessageSerializer implements IMessageSerializer
{

    static XStream _xStream;
	XMLEncoder encoder=null;
	XMLDecoder decoder=null;




	public final byte[] Encode(Message message)
	{
		if (message == null)
		{
			try {
				throw new Exception("Cannot encode a null message");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}


	   String xmlString=_xStream.toXML(message);

		return xmlString.getBytes();
	}

	public final Message Decode(byte[] bytes) throws Exception {
		if (bytes == null)
		{
			throw new Exception("Cannot decode an empty byte array");
		}

		ByteBuffer byteBuffer=ByteBuffer.wrap(bytes);
		Object tempVar =_xStream.fromXML(byteBuffer.toString());
		return (Message)((tempVar instanceof Message) ? tempVar : null);
	}
}